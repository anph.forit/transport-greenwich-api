<?php

namespace App\Containers\Ticket\Models;

use App\Containers\Boat\Models\Schedule;
use App\Containers\User\Models\User;
use App\Ship\Parents\Models\Model;

class Ticket extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'schedule_id',
        'seat',
        'timeval',
        'pick_date',
        'payment_method',
        'payment_status',
        'payment_token',
        'status',
        'qr_code',
        'price'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function schedule()
    {
        return $this->hasOne(Schedule::class, 'id', 'schedule_id');
    }
}
