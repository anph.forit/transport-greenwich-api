<?php

namespace App\Containers\Ticket\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class GetDashboardInfoAction extends Action
{
    public function run()
    {
        $info = Apiato::call('Ticket@GetDashboardInfoTask');

        return $info;
    }
}
