<?php

namespace App\Containers\Ticket\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class FindTicketAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'id'                => $data->id,
            'user_id'           => $data->user_id,
            'schedule_id'       => $data->schedule_id,
            'seat'              => $data->seat,
            'timeval'           => $data->timeval,
            'pick_date'         => $data->pick_date,
            'payment_method'    => $data->payment_method,
            'status'            => $data->status,
          ];
        $ticket = Apiato::call('Ticket@FindTicketTask', [$params]);

        return $ticket;
    }
}
