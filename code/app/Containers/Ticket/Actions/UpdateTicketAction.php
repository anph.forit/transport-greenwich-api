<?php

namespace App\Containers\Ticket\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateTicketAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'id'                => $data->id,
            'schedule_id'       => $data->schedule_id,
            'seat'              => $data->seat,
            'timeval'           => $data->timeval,
            'pick_date'         => $data->pick_date,
            'payment_method'    => $data->payment_method,
            'payment_status'    => $data->payment_status,
            'payment_token'     => $data->payment_token,
            'status'            => $data->status,
            'price'             => $data->price,
          ];
          // remove empty value and key
          $params = array_filter($params, function($x) { return !is_null($x); });

        $ticket = Apiato::call('Ticket@UpdateTicketTask', [$params]);

        return $ticket;
    }
}
