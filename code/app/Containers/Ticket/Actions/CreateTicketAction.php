<?php

namespace App\Containers\Ticket\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateTicketAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'user_id'           => $data->user_id,
            'schedule_id'       => $data->schedule_id,
            'seat'              => $data->seat,
            'timeval'           => $data->timeval,
            'pick_date'         => $data->pick_date,
            'payment_method'    => $data->payment_method,
            'price'             => $data->price,
          ];
          // remove empty value and key
        $params = array_filter($params);

        $ticket = Apiato::call('Ticket@CreateTicketTask', [$params]);

        return $ticket;
    }
}
