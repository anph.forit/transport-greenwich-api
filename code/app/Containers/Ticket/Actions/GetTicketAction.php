<?php

namespace App\Containers\Ticket\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class GetTicketAction extends Action
{
    public function run(DataTransporter $data)
    {
        $tickets = Apiato::call('Ticket@GetTicketTask');

        return $tickets;
    }
}
