<?php

namespace App\Containers\Ticket\Tasks;

use App\Containers\Ticket\Data\Repositories\TicketRepository;
use App\Ship\Parents\Tasks\Task;

class GetTicketTask extends Task
{

    protected $repository;

    public function __construct(TicketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->with('user')->with('schedule')->paginate();
    }
}
