<?php

namespace App\Containers\Ticket\Tasks;

use App\Containers\Ticket\Data\Repositories\TicketRepository;
use App\Containers\Ticket\Models\Ticket;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateTicketTask extends Task
{

    protected $repository;

    public function __construct(TicketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        if (empty($data)) {
            throw new UpdateResourceFailedException('Inputs are empty.');
          }
          try {
              if (isset($data['payment_token']) && $data['payment_token'] != null){
                return Ticket::where('payment_token', $data['payment_token'])->update(array_filter($data, function($var){return $var !== null;}));
              }
              return Ticket::where('id', $data['id'])->update(array_filter($data, function($var){return $var !== null;}));
          } catch (Exception $exception) {
              throw new UpdateResourceFailedException();
          }
          return NULL;
    }
}
