<?php

namespace App\Containers\Ticket\Tasks;

use App\Containers\Boat\Models\Schedule;
use App\Containers\Ticket\Data\Repositories\TicketRepository;
use App\Containers\Ticket\Models\Ticket;
use App\Containers\User\Models\User;
use App\Ship\Parents\Tasks\Task;

class GetDashboardInfoTask extends Task
{

    protected $repository;

    public function __construct(TicketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $res = new \stdClass;
        $earnMonth = Ticket::whereMonth('created_at', date('m'))->where('status', 'DONE')->sum('price');
        $mostscheduleid = Ticket::select('schedules.departure_station_id')
            ->join("schedules", "schedules.id", "=", "tickets.schedule_id")
            ->groupBy('schedules.departure_station_id')->orderByRaw('COUNT(*) DESC')->first();
        $leastscheduleid = Ticket::select('schedules.departure_station_id')
            ->join("schedules", "schedules.id", "=", "tickets.schedule_id")
            ->groupBy('schedules.departure_station_id')->orderByRaw('COUNT(*) ASC')->first();        
        $res->earn_monthly = $earnMonth;
        if ($mostscheduleid) {
            $mostschedule = Schedule::where('departure_station_id',$mostscheduleid->departure_station_id)->with('departure_station')->with('arrival_station')->first();
            $res->most_schedule = $mostschedule->departure_station->station_name . ' - ' . $mostschedule->arrival_station->station_name;
        }
        if ($leastscheduleid) {
            $leastschedule = Schedule::where('departure_station_id',$leastscheduleid->departure_station_id)->with('departure_station')->with('arrival_station')->first();
            $res->least_schedule = $leastschedule->departure_station->station_name . ' - ' . $leastschedule->arrival_station->station_name;
        }
        $userNo = User::where('is_client', 1)->count();
        $res->user_number = $userNo;
        return $res;
    }
}
