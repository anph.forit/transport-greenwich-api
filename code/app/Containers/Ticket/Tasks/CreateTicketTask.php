<?php

namespace App\Containers\Ticket\Tasks;

use App\Containers\Ticket\Data\Repositories\TicketRepository;
use App\Containers\Ticket\Services\PayPalService;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Exception;

class CreateTicketTask extends Task
{

    protected $repository;

    public function __construct(TicketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        $payPalService = app(PayPalService::class);
        $data["payment_status"] = "WAITING_PAID";
        $data["status"] = "WAITING";
        DB::beginTransaction();
        try {
            $res = $this->repository->create($data);
            $res->qr_code = $this->normalizeQR(QrCode::size(200)->margin(0)->generate($res->id));
            $paypalCheckoutUrl = '';
            if ($data["payment_method"] == 'Paypal') {
                $data = [
                    [
                        'name' => 'Shipment Trip',
                        'quantity' => 1,
                        'price' => $res->price,
                        'sku' => $res->id
                    ]
                ];
                
                $transactionDescription = "Tobaco";

                $paypalCheckoutUrl = $payPalService
                    // ->setCurrency('eur')
                    ->setReturnUrl(url('v1/ticket/status'))
                    // ->setCancelUrl(url('paypal/status'))
                    ->setItem($data)
                    ->createPayment($transactionDescription);
                $res->payment_token = substr($paypalCheckoutUrl, strpos($paypalCheckoutUrl,"token=") + 6, strlen($paypalCheckoutUrl)-1);
            }
            $res->save();
            DB::commit();
            return [
                'ticket'       => $res,
                'payment_url' => $paypalCheckoutUrl,
            ];
        } catch (Exception $exception) {
            DB::rollback();
            Log::error($exception);
            throw new CreateResourceFailedException();
        }
    }

    private function normalizeQR($text)
    {
        $text = str_replace("\n", "", $text);
        $text = str_replace('"', "'", $text);
        return $text;
    }
}
