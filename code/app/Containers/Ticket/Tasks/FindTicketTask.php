<?php

namespace App\Containers\Ticket\Tasks;

use App\Containers\Ticket\Data\Repositories\TicketRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindTicketTask extends Task
{

    protected $repository;

    public function __construct(TicketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $val)
    {
        if (isset($val['id']) && $val['id'] != null){
            $ticketlst =  $this->repository->with('user')->with('schedule')->findWhere([
              'id' => $val['id']
            ]);
          }
          else{
            $sqlstr = "";
            $chk = 0;
            if (isset($val['user_id']) && $val['user_id'] != null){
              $sqlstr = " and user_id = '".$val['user_id']."'";
              $chk = 1;
            }
            if (isset($val['schedule_id']) && $val['schedule_id'] != null){
              $sqlstr = $sqlstr." and schedule_id = '".$val['schedule_id']."'";
              $chk = 1;
            }
            if (isset($val['timeval']) && $val['timeval'] != null){
              $sqlstr = $sqlstr." and timeval = '".$val['timeval']."'";
              $chk = 1;
            }
            if (isset($val['pick_date']) && $val['pick_date'] != null){
                $sqlstr = $sqlstr." and pick_date = '".$val['pick_date']."'";
                $chk = 1;
              }
            if (isset($val['seat']) && $val['seat'] != null){
              $sqlstr = $sqlstr." and seat LIKE '%".$val['seat']."%'";
              $chk = 1;
            }
            if (isset($val['payment_method']) && $val['payment_method'] != null){
                $sqlstr = $sqlstr." and payment_method = '".$val['payment_method']."'";
                $chk = 1;
              }
            if (isset($val['status']) && $val['status'] != null){
                $sqlstr = $sqlstr." and status = '".$val['status']."'";
                $chk = 1;
            }
            if ($chk == 1){
              $sqlstr = substr($sqlstr, 5, strlen($sqlstr) - 1);
            }
            $ticketlst = $this->repository->with('user')->with('schedule')->scopeQuery(function ($query) use($sqlstr) {
                return $query->where(function ($sq) use ($sqlstr) {
                    $sq->whereRaw($sqlstr);});})->all();
          }
          if ($ticketlst)
            return $ticketlst;
          return NULL;
    }
}
