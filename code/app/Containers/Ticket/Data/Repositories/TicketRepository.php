<?php

namespace App\Containers\Ticket\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TicketRepository
 */
class TicketRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
