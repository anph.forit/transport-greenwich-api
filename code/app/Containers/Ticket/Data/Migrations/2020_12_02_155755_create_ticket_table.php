<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('schedule_id');
            $table->string('seat');
            $table->string('timeval');
            $table->date('pick_date');
            $table->string('payment_method');
            $table->string('payment_status'); // WAITING_PAID, PAID, REFUND
            $table->string('payment_token')->nullable(); 
            $table->string('status'); // DONE, CANCEL, WAITING, REFUND
            $table->text('qr_code')->nullable();           
            $table->decimal('price', 10, 0);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
