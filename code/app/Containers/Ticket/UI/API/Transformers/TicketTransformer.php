<?php

namespace App\Containers\Ticket\UI\API\Transformers;

use App\Containers\Ticket\Models\Ticket;
use App\Containers\User\UI\API\Transformers\UserTransformer;
use App\Ship\Parents\Transformers\Transformer;

class TicketTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [
        'user', 'schedule'
    ];

    /**
     * @param Ticket $entity
     *
     * @return array
     */
    public function transform(Ticket $entity)
    {
        $response = [
            'object' => 'Ticket',
            'id' => $entity->id,
            'user_id' => $entity->user_id,
            'schedule_id' => $entity->schedule_id,
            'seat' => $entity->seat,
            'timeval' => $entity->timeval,
            'pick_date' => $entity->pick_date,
            'payment_method' => $entity->payment_method,
            'payment_status' => $entity->payment_status,
            'payment_token' => $entity->payment_token,
            'status' => $entity->status,
            'qr_code' => $entity->qr_code,
            'price' => $entity->price,
            'user' => $entity->user,
            'schedule' => $entity->schedule,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }

    public function includeUser(Ticket $ticket)
    {
        return $this->collection($ticket->user, new UserTransformer());
    }
}
