<?php

namespace App\Containers\Ticket\UI\API\Controllers;

use App\Containers\Ticket\Services\PayPalService;
use App\Containers\Ticket\UI\API\Requests\CreateTicketRequest;
use App\Containers\Ticket\UI\API\Requests\DeleteTicketRequest;
use App\Containers\Ticket\UI\API\Requests\GetTicketRequest;
use App\Containers\Ticket\UI\API\Requests\FindTicketRequest;
use App\Containers\Ticket\UI\API\Requests\UpdateTicketRequest;
use App\Containers\Ticket\UI\API\Transformers\TicketTransformer;
use App\Containers\Ticket\UI\API\Requests\GetDashboardInfoRequest;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Http\Request;

/**
 * Class Controller
 *
 * @package App\Containers\Ticket\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTicket(CreateTicketRequest $request)
    {
        $ticket = Apiato::call('Ticket@CreateTicketAction', [new DataTransporter($request)]);
        return $this->json($ticket);
    }

    public function ticketStatus(Request $request)
    {
        $res = $request->all();        
        if($res['paymentId']) {
            $params = [
                'payment_token'     => $res['token'],
                'payment_status'    => 'PAID',
              ];
            $ticket = Apiato::call('Ticket@UpdateTicketTask', [$params]);
        }
      return redirect('/');
    }

    /**
     * @param FindTicketByIdRequest $request
     * @return array
     */
    public function findTicket(FindTicketRequest $request)
    {
        $ticket = Apiato::call('Ticket@FindTicketAction', [new DataTransporter($request)]);

        return $this->json($ticket);
    }

    /**
     * @param GetAllTicketsRequest $request
     * @return array
     */
    public function getTicket(GetTicketRequest $request)
    {
        $tickets = Apiato::call('Ticket@GetTicketAction', [new DataTransporter($request)]);
        return $this->transform($tickets, TicketTransformer::class);
    }

    /**
     * @param UpdateTicketRequest $request
     * @return array
     */
    public function updateTicket(UpdateTicketRequest $request)
    {
        $ticket = Apiato::call('Ticket@UpdateTicketAction', [new DataTransporter($request)]);

        return $this->json($ticket);
    }

    /**
     * @param DeleteTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTicket(DeleteTicketRequest $request)
    {
        Apiato::call('Ticket@DeleteTicketAction', [$request]);

        return $this->noContent();
    }

    public function getDashboardInfo(GetDashboardInfoRequest $request){
        $info = Apiato::call('Ticket@GetDashboardInfoAction');
        return $this->json($info);
    }
}
