<?php

/**
 * @apiGroup           Ticket
 * @apiName            deleteTicket
 *
 * @api                {DELETE} /v1/ticket/delete_ticket Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('ticket/delete_ticket', [
    'as' => 'api_ticket_delete_ticket',
    'uses'  => 'Controller@deleteTicket',
    'middleware' => [
      'auth:api',
    ],
]);
