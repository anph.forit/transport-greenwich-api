<?php

/**
 * @apiGroup           Ticket
 * @apiName            createTicket
 *
 * @api                {POST} /v1/ticket/create_ticket Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('ticket/create_ticket', [
    'as' => 'api_ticket_create_ticket',
    'uses'  => 'Controller@createTicket',
    'middleware' => [
      'auth:api',
    ],
]);

$router->get('ticket/status', [
    'as' => 'api_ticket_status_ticket',
    'uses'  => 'Controller@ticketStatus'
]);
