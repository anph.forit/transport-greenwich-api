<?php

/**
 * @apiGroup           Ticket
 * @apiName            getDashboardInfo
 *
 * @api                {GET} /v1/getdashboardinfo Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('getdashboardinfo', [
    'as' => 'api_ticket_get_dashboard_info',
    'uses'  => 'Controller@getDashboardInfo',
    'middleware' => [
      'auth:api',
    ],
]);
