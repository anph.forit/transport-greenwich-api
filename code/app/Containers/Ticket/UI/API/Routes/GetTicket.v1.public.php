<?php

/**
 * @apiGroup           Ticket
 * @apiName            getTicket
 *
 * @api                {GET} /v1/ticket/get_ticket Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('ticket/get_ticket', [
    'as' => 'api_ticket_get_ticket',
    'uses'  => 'Controller@getTicket',
    'middleware' => [
      'auth:api',
    ],
]);
