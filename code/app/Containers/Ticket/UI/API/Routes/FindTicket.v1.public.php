<?php

/**
 * @apiGroup           Ticket
 * @apiName            findTicket
 *
 * @api                {GET} /v1/ticket/find_ticket Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('ticket/find_ticket', [
    'as' => 'api_ticket_find_ticket',
    'uses'  => 'Controller@findTicket',
    'middleware' => [
      'auth:api',
    ],
]);
