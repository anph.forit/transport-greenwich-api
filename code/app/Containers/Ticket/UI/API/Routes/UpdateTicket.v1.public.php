<?php

/**
 * @apiGroup           Ticket
 * @apiName            updateTicket
 *
 * @api                {PUT} /v1/ticket/update_ticket Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->put('ticket/update_ticket', [
    'as' => 'api_ticket_update_ticket',
    'uses'  => 'Controller@updateTicket',
    'middleware' => [
      'auth:api',
    ],
]);
