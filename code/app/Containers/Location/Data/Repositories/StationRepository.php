<?php

namespace App\Containers\Location\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class StationRepository.
 *
 * @author
 */
class StationRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id'            => '=',
        'station_name'  => 'like',
        'loc_id'        => '=',
        'active'        => '=',
    ];

}
