<?php

namespace App\Containers\Location\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class LocationRepository.
 *
 * @author
 */
class LocationRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id'                => '=',
        'loc_code'          => '=',
        'loc_name'          => 'like',
        'active'            => '=',
    ];

}
