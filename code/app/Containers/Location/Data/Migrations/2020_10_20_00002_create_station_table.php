<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStationTable extends Migration
{

  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('station', function (Blueprint $table) {
      $table->increments('id');
      $table->string('station_name');
      $table->string('description')->default("")->nullable();
      $table->string('loc_id')->nullable();
      $table->boolean('active')->default(true);
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
    Schema::drop('location');
  }
}
