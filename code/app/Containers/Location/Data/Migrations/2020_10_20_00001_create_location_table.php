<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationTable extends Migration
{

  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('location', function (Blueprint $table) {
      $table->increments('id');
      $table->string('loc_code')->unique();
      $table->string('loc_name');
      $table->string('description')->default("")->nullable();
      $table->boolean('active')->default(true);
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
    Schema::drop('location');
  }
}
