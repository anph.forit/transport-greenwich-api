<?php

namespace App\Containers\Location\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Location\Data\Repositories\StationRepository;

/**
 * Class SearchLocationTask.
 *
 * @author
 */
class SearchStationTask extends Task
{
  private $repository;
  public function __construct(StationRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(array $val) {
    if (isset($val['id']) && $val['id'] != null){
      $stalst =  $this->repository->findWhere([
        'id' => $val['id']
      ]);
    }
    else{
      if ($val['val'] == "all"){
        $stalst =  $this->repository->with('location')->findWhere([
          'active' => true
        ]);
      }
      else{
        $stalst = $this->repository->with('location')->scopeQuery(function ($query) use($val) {
        return $query->where(function ($sq) use ($val) {
          $sq->whereRaw("LOWER(station_name) LIKE '%".strtolower($val['val'])."%'");
        })->where('active', true);})->all();
      }
    }
    if ($stalst)
      return $stalst;    
    return NULL;
  }
}
