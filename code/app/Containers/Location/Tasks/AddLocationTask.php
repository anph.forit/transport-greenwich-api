<?php

namespace App\Containers\Location\Tasks;

use App\Ship\Parents\Exceptions\Exception;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Location\Data\Repositories\LocationRepository;

/**
 * Class AddLocationTask.
 *
 * @author
 */
class AddLocationTask extends Task
{
  private $repository;
  public function __construct(LocationRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run($data) {
    if (empty($data)) {
      throw new CreateResourceFailedException('Inputs are empty.');
    }
    try {
      return $this->repository->create($data);
    } catch (Exception $exception) {
      var_dump($exception);

        throw new CreateResourceFailedException();
    }
    return NULL;
  }
}
