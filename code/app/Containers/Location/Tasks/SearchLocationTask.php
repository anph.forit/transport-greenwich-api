<?php

namespace App\Containers\Location\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Location\Data\Repositories\LocationRepository;

/**
 * Class SearchLocationTask.
 *
 * @author
 */
class SearchLocationTask extends Task
{
  private $repository;
  public function __construct(LocationRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(array $val) {
    if (isset($val['id']) && $val['id'] != null){
      $loclst =  $this->repository->findWhere([
        'id' => $val['id']
      ]);
    }
    else{
      if ($val['val'] == "all"){
        $loclst =  $this->repository->findWhere([
          'active' => true
        ]);
      }
      else{
        $loclst = $this->repository->scopeQuery(function ($query) use($val) {
          return $query->where(function ($sq) use ($val) {
              $sq->whereRaw("LOWER(loc_name) LIKE '%".strtolower($val['val'])."%'");
          })->where('active', true);})->all();
      }
      
    }
    if ($loclst)
      return $loclst;
    return NULL;
  }
}
