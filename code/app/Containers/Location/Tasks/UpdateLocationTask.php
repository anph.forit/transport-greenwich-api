<?php

namespace App\Containers\Location\Tasks;

use App\Ship\Parents\Exceptions\Exception;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Containers\Location\Models\Location;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Location\Data\Repositories\LocationRepository;

/**
 * Class UpdateLocationTask.
 *
 * @author
 */
class UpdateLocationTask extends Task
{
  private $repository;
  public function __construct(LocationRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run($data) {
    if (empty($data)) {
      throw new UpdateResourceFailedException('Inputs are empty.');
    }
    try {
        if (!is_null($data['id'])){
          return Location::where('id', $data['id'])->update(array_filter($data, function($var){return $var !== null;}));
        }
        // else if (!is_null($data['parent_id'])){
        //   return Location::where('parent_id', $data['parent_id'])->update(array_filter($data, function($var){return $var !== null;}));
        // }
        
    } catch (Exception $exception) {
        throw new UpdateResourceFailedException();
    }
    return NULL;
  }
}
