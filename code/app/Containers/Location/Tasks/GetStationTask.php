<?php

namespace App\Containers\Location\Tasks;

use App\Containers\Location\Data\Repositories\StationRepository;
use App\Ship\Parents\Tasks\Task;

class GetStationTask extends Task
{

    protected $repository;

    public function __construct(StationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->with('location')->paginate();
    }
}
