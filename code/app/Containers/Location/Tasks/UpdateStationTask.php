<?php

namespace App\Containers\Location\Tasks;

use App\Ship\Parents\Exceptions\Exception;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Containers\Location\Models\Station;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Location\Data\Repositories\StationRepository;

/**
 * Class UpdateStationTask.
 *
 * @author
 */
class UpdateStationTask extends Task
{
  private $repository;
  public function __construct(StationRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run($data) {
    if (empty($data)) {
      throw new UpdateResourceFailedException('Inputs are empty.');
    }
    try {
        return Station::where('id', $data['id'])->update(array_filter($data, function($var){return $var !== null;}));
    } catch (Exception $exception) {
        throw new UpdateResourceFailedException();
    }
    return NULL;
  }
}
