<?php

namespace App\Containers\Location\UI\API\Transformers;

use App\Containers\Location\Models\Location;
use App\Ship\Parents\Transformers\Transformer;

class LocationTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Location $entity
     *
     * @return array
     */
    public function transform(Location $entity)
    {
        $response = [
            'object' => 'Location',
            'id' => $entity->id,
            'loc_name' => $entity->loc_name,
            'loc_code' => $entity->loc_code,
            'description' => $entity->description,
            'insensitive_name' => $entity->insensitive_name,
            'type' => $entity->type,
            'active' => $entity->active,
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
