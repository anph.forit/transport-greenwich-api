<?php

namespace App\Containers\Location\UI\API\Transformers;
use App\Containers\Location\UI\API\Transformers\LocationTransformer;
use App\Containers\Location\Models\Station;
use App\Ship\Parents\Transformers\Transformer;

class StationTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [
        'location',
    ];

    /**
     * @param Station $entity
     *
     * @return array
     */
    public function transform(Station $entity)
    {
        $response = [
            'object' => 'Station',
            'id' => $entity->id,
            'station_name' => $entity->station_name,
            'location' => $entity->location,
            'loc_id' => $entity->loc_id,
            'description' => $entity->description,
            'active' => $entity->active,
        ];
        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }

    public function includeLocation(Station $station)
    {
        return $this->collection($station->location, new LocationTransformer());
    }
}
