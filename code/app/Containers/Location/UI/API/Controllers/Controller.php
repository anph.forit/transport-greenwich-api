<?php

namespace App\Containers\Location\UI\API\Controllers;

use App\Containers\Location\UI\API\Requests\AddLocationRequest;
use App\Containers\Location\UI\API\Requests\AddStationRequest;
use App\Containers\Location\UI\API\Requests\SearchLocationRequest;
use App\Containers\Location\UI\API\Requests\SearchStationRequest;
use App\Containers\Location\UI\API\Requests\GetLocationRequest;
use App\Containers\Location\UI\API\Requests\GetStationRequest;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Location\UI\API\Transformers\StationTransformer;
use App\Containers\Location\UI\API\Requests\UpdateLocationRequest;
use App\Containers\Location\UI\API\Requests\UpdateStationRequest;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller
 *
 * @author
 */
class Controller extends ApiController
{
  /**
   * @param AddLocationRequest $request
   * @return array
   */
  public function addLocation(AddLocationRequest $request)
  {
    return Apiato::call('Location@AddLocationAction', [new DataTransporter($request)]);
  }

  /**
   * @param AddStationRequest $request
   * @return array
   */
  public function addStation(AddStationRequest $request)
  {
    $station = Apiato::call('Location@AddStationAction', [new DataTransporter($request)]);
    return $this->json($station);
  }

  /**
   * @param GetLocationRequest $request
   * @return array
   */
  public function getLocation(GetLocationRequest $request)
  {
    return Apiato::call('Location@GetLocationAction', [new DataTransporter($request)]);
  }

  /**
   * @param GetStationRequest $request
   * @return array
   */
  public function getStation(GetStationRequest $request)
  {
    $station = Apiato::call('Location@GetStationAction', [new DataTransporter($request)]);
    return $this->transform($station, StationTransformer::class);
  }

  /**
   * @param UpdateLocationRequest $request
   * @return array
   */
  public function updateLocation(UpdateLocationRequest $request)
  {
    return Apiato::call('Location@UpdateLocationAction', [new DataTransporter($request)]);
  }

  /**
   * @param UpdateStationRequest $request
   * @return array
   */
  public function updateStation(UpdateStationRequest $request)
  {
    $station = Apiato::call('Location@UpdateStationAction', [new DataTransporter($request)]);
    return $this->json($station);
  }

  /**
   * @param SearchLocationRequest $request
   * @return array
   */
  public function searchLocation(SearchLocationRequest $request)
  {
    $location = Apiato::call('Location@SearchLocationAction', [new DataTransporter($request)]);
    return $this->json($location);
  }

  /**
   * @param SearchStationRequest $request
   * @return array
   */
  public function searchStation(SearchStationRequest $request)
  {
    $station = Apiato::call('Location@SearchStationAction', [new DataTransporter($request)]);
    return $this->json($station);
  }
}
