<?php


/**
 * @apiGroup           Location
 * @apiName            addStation
 *
 * @api                {POST} /v1/station
 * @apiDescription     add station
 *
 * @apiVersion         1.0.0
 * @apiPermission      admin
 *
 * @apiParam           {Int}  loc_id
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->post('/station', [
  'as' => 'api_add_station',
  'uses' => 'Controller@addStation',
  'middleware' => [
     'auth:api',
  ],
]);
