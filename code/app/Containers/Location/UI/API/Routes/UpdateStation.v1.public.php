<?php


/**
 * @apiGroup           Location
 * @apiName            updateStation
 *
 * @api                {PUT} /v1/station
 * @apiDescription     update station data
 *
 * @apiVersion         1.0.0
 * @apiPermission      admin
 *
 * @apiParam           {Int}   id
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->put('/station', [
  'as' => 'api_update_station',
  'uses' => 'Controller@updateStation',
  'middleware' => [
     'auth:api',
  ],
]);
