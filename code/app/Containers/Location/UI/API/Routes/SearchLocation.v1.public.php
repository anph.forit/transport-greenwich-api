<?php


/**
 * @apiGroup           Location
 * @apiName            SearchLocation
 *
 * @api                {GET} /v1/searchlocation
 * @apiDescription     search location
 *
 * @apiVersion         1.0.0
 * @apiPermission
 *
 * @apiParam           {string} name
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->get('/searchlocation', [
  'as' => 'api_search_loc',
  'uses' => 'Controller@searchLocation',
  'middleware' => [
  ],
]);
