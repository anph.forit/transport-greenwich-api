<?php


/**
 * @apiGroup           Location
 * @apiName            addLocation
 *
 * @api                {POST} /v1/location
 * @apiDescription     add location
 *
 * @apiVersion         1.0.0
 * @apiPermission      admin
 *
 * @apiParam           {Int}  loc_id
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->post('/location', [
  'as' => 'api_add_location',
  'uses' => 'Controller@addLocation',
  'middleware' => [
     'auth:api',
  ],
]);
