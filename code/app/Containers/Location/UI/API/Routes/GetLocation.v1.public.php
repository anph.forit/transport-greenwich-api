<?php


/**
 * @apiGroup           Location
 * @apiName            getLocation
 *
 * @api                {GET} /v1/location
 * @apiDescription     get location
 *
 * @apiVersion         1.0.0
 * @apiPermission      admin
 *
 * @apiParam           {Int}  loc_id
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->get('/location', [
  'as' => 'api_get_location',
  'uses' => 'Controller@getLocation',
  'middleware' => [
     'auth:api',
  ],
]);
