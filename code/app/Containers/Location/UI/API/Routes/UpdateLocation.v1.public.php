<?php


/**
 * @apiGroup           Location
 * @apiName            updateLocation
 *
 * @api                {PUT} /v1/location
 * @apiDescription     update location
 *
 * @apiVersion         1.0.0
 * @apiPermission      admin
 *
 * @apiParam           {Int}  loc_id
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->put('/location', [
  'as' => 'api_update_location',
  'uses' => 'Controller@updateLocation',
  'middleware' => [
     'auth:api',
  ],
]);
