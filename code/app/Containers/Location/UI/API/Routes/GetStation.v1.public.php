<?php


/**
 * @apiGroup           Location
 * @apiName            getStation
 *
 * @api                {GET} /v1/station
 * @apiDescription     get station
 *
 * @apiVersion         1.0.0
 * @apiPermission      admin
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->get('/station', [
  'as' => 'api_get_station',
  'uses' => 'Controller@getStation',
  'middleware' => [
     'auth:api',
  ],
]);
