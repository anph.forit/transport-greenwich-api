<?php


/**
 * @apiGroup           Location
 * @apiName            searchStation
 *
 * @api                {GET} /v1/searchstation
 * @apiDescription     search station
 *
 * @apiVersion         1.0.0
 * @apiPermission
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 * {
 * }
 */

$router->get('/searchstation', [
  'as' => 'api_search_station',
  'uses' => 'Controller@searchStation',
  'middleware' => [
  ],
]);
