<?php

namespace App\Containers\Location\UI\API\Requests;

use App\Ship\Parents\Requests\Request;

class AddLocationRequest extends Request
{

  /**
   * Define which Roles and/or Permissions has access to this request.
   *
   * @var  array
   */
  protected $access = [
    'permissions' => '',
    'roles'       => 'admin',
  ];

  /**
   * Id's that needs decoding before applying the validation rules.
   *
   * @var  array
   */
  protected $decode = [
  ];

  /**
   * Defining the URL parameters (`/stores/999/items`) allows applying
   * validation rules on them and allows accessing them like request data.
   *
   * @var  array
   */
  protected $urlParameters = [
  ];

  /**
   * @return  array
   */
  public function rules()
  {
    return [
        'loc_name' => 'required',
        'loc_code' => 'required',
    ];
  }

  /**
   * @return  bool
   */
  public function authorize()
  {
    return $this->check([
      'hasAccess',
    ]);
  }
}
