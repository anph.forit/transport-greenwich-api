<?php

namespace App\Containers\Location\Models;

use App\Ship\Parents\Models\Model;

class Location extends Model
{
    protected $table = 'location';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'loc_name',
        'loc_code',
        'description',
        'insensitive_name',
        'parent_id',
        'type',
        'active',
    ];

    protected $attributes = [
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    protected $dates = [
    ];    
}
