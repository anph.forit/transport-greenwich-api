<?php

namespace App\Containers\Location\Models;

use App\Ship\Parents\Models\Model;

class Station extends Model
{
    protected $table = 'station';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'station_name',
        'description',        
        'loc_id',
        'active',
    ];

    protected $attributes = [
    ];

    protected $hidden = [
    ];

    protected $casts = [
    ];

    protected $dates = [
    ];

    public function location()
    {
        return $this->hasOne(Location::class, 'id', 'loc_id');
    }
}
