<?php

namespace App\Containers\Location\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class SearchStationAction.
 *
 * @author
 */
class SearchStationAction extends Action
{
  /**
   * @param \App\Ship\Transporters\DataTransporter $data
   */
  public function run(DataTransporter $data)
  {
    $params = [
      'id'       => $data->id,
      'val' => $data->val,
    ];
    // remove empty value and key
    $params = array_filter($params);
    return Apiato::call('Location@SearchStationTask', [
      $params
    ]);
  }
}
