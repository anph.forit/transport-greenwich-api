<?php

namespace App\Containers\Location\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetStationAction.
 *
 * @author
 */
class GetStationAction extends Action
{
  /**
   * @param \App\Ship\Transporters\DataTransporter $data
   */
  public function run(DataTransporter $data)
  {
    return Apiato::call('Location@GetStationTask');
  }
}
