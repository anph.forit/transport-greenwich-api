<?php

namespace App\Containers\Location\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class AddLocationAction.
 *
 * @author
 */
class AddStationAction extends Action
{
  /**
   * @param \App\Ship\Transporters\DataTransporter $data
   */
  public function run(DataTransporter $data)
  {
    $params = [
      'station_name'  => $data->station_name,
      'loc_id'        => $data->loc_id,
      'description'   => $data->description
    ];
    // remove empty value and key
    $params = array_filter($params);
    return Apiato::call('Location@AddStationTask', [$params]);
  }
}
