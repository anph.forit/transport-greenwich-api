<?php

namespace App\Containers\Boat\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class BoatRepository
 */
class BoatRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
