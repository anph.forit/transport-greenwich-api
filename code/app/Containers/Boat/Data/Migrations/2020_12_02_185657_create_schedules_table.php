<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchedulesTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('boat_id');
            $table->string('timevals');
            $table->integer('departure_station_id');
            $table->integer('arrival_station_id');
            $table->string('days_of_week');
            $table->decimal('price', 8, 0);
            $table->text('description')->default("")->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
            //$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
