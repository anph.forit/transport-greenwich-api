<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBoatTable extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('boats', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->integer('num_of_seats');
            $table->boolean('active')->default(true);
            $table->text('description')->default("")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('boats');
    }
}
