<?php

/**
 * @apiGroup           Boat
 * @apiName            findSchedule
 *
 * @api                {GET} /v1/schedule/find_schedule Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('schedule/find_schedule', [
    'as' => 'api_boat_find_schedule',
    'uses'  => 'Controller@findSchedule',
    'middleware' => [
      'auth:api',
    ],
]);
