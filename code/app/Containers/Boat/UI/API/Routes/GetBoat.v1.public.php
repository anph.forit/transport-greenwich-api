<?php

/**
 * @apiGroup           Boat
 * @apiName            getBoat
 *
 * @api                {GET} /v1/boat/get_boat Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('boat/get_boat', [
    'as' => 'api_boat_get_boat',
    'uses'  => 'Controller@getBoat',
    'middleware' => [
      'auth:api',
    ],
]);
