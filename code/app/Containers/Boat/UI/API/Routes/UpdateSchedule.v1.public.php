<?php

/**
 * @apiGroup           Boat
 * @apiName            updateSchedule
 *
 * @api                {PUT} /v1/schedule/update_schedule Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->put('schedule/update_schedule', [
    'as' => 'api_boat_update_schedule',
    'uses'  => 'Controller@updateSchedule',
    'middleware' => [
      'auth:api',
    ],
]);
