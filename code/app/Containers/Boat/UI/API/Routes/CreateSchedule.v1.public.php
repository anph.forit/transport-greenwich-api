<?php

/**
 * @apiGroup           Boat
 * @apiName            createSchedule
 *
 * @api                {POST} /v1/schedule/create_schedule Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('schedule/create_schedule', [
    'as' => 'api_boat_create_schedule',
    'uses'  => 'Controller@createSchedule',
    'middleware' => [
      'auth:api',
    ],
]);
