<?php

/**
 * @apiGroup           Boat
 * @apiName            deleteSchedule
 *
 * @api                {DELETE} /v1/schedule/delete_schedule Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('schedule/delete_schedule', [
    'as' => 'api_boat_delete_schedule',
    'uses'  => 'Controller@deleteSchedule',
    'middleware' => [
      'auth:api',
    ],
]);
