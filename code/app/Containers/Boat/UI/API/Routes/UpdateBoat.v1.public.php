<?php

/**
 * @apiGroup           Boat
 * @apiName            updateBoat
 *
 * @api                {PUT} /v1/boat/update_boat Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->put('boat/update_boat', [
    'as' => 'api_boat_update_boat',
    'uses'  => 'Controller@updateBoat',
    'middleware' => [
      'auth:api',
    ],
]);
