<?php

/**
 * @apiGroup           Boat
 * @apiName            deleteSeat
 *
 * @api                {DELETE} /v1/seat/delete_seat Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('boat/delete_boat', [
    'as' => 'api_boat_delete_boat',
    'uses'  => 'Controller@deleteBoat',
    'middleware' => [
      'auth:api',
    ],
]);
