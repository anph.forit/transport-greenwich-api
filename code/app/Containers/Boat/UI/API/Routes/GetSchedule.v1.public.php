<?php

/**
 * @apiGroup           Boat
 * @apiName            getSchedule
 *
 * @api                {GET} /v1/schedule/get_schedule Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('schedule/get_schedule', [
    'as' => 'api_boat_get_schedule',
    'uses'  => 'Controller@getSchedule',
    'middleware' => [
      'auth:api',
    ],
]);
