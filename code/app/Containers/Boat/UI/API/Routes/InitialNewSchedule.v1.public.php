<?php

/**
 * @apiGroup           Boat
 * @apiName            findSchedule
 *
 * @api                {GET} /v1/schedule/find_schedule Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('schedule/initial_new_schedule', [
    'as' => 'api_boat_initial_schedule',
    'uses'  => 'Controller@initialNewSchedule',
    'middleware' => [
      'auth:api',
    ],
]);
