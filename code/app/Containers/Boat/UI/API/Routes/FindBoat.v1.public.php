<?php

/**
 * @apiGroup           Boat
 * @apiName            findBoat
 *
 * @api                {GET} /v1/boat/find_boat Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('boat/find_boat', [
    'as' => 'api_boat_find_boat',
    'uses'  => 'Controller@findBoat',
    'middleware' => [
      'auth:api',
    ],
]);
