<?php

namespace App\Containers\Boat\UI\API\Controllers;

use App\Containers\Boat\UI\API\Requests\CreateBoatRequest;
use App\Containers\Boat\UI\API\Requests\DeleteBoatRequest;
use App\Containers\Boat\UI\API\Requests\GetBoatRequest;
use App\Containers\Boat\UI\API\Requests\FindBoatRequest;
use App\Containers\Boat\UI\API\Requests\UpdateBoatRequest;
use App\Containers\Boat\UI\API\Transformers\BoatTransformer;
use App\Containers\Boat\UI\API\Requests\InitialNewScheduleRequest;
use App\Containers\Boat\UI\API\Requests\CreateScheduleRequest;
use App\Containers\Boat\UI\API\Requests\DeleteScheduleRequest;
use App\Containers\Boat\UI\API\Requests\GetScheduleRequest;
use App\Containers\Boat\UI\API\Requests\FindScheduleRequest;
use App\Containers\Boat\UI\API\Requests\UpdateScheduleRequest;
use App\Containers\Boat\UI\API\Transformers\ScheduleTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller
 *
 * @package App\Containers\Boat\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateBoatRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBoat(CreateBoatRequest $request)
    {
        $boat = Apiato::call('Boat@CreateBoatAction', [new DataTransporter($request)]);

        return $this->created($this->transform($boat, BoatTransformer::class));
    }

    /**
     * @param FindBoatRequest $request
     * @return array
     */
    public function findBoat(FindBoatRequest $request)
    {
        $boat = Apiato::call('Boat@FindBoatAction', [new DataTransporter($request)]);

        return $this->json($boat);
    }

    /**
     * @param GetBoatRequest $request
     * @return array
     */
    public function getBoat(GetBoatRequest $request)
    {
        $boats = Apiato::call('Boat@GetBoatAction', [new DataTransporter($request)]);

        return $this->json($boats);
    }

    /**
     * @param UpdateBoatRequest $request
     * @return array
     */
    public function updateBoat(UpdateBoatRequest $request)
    {
        $boat = Apiato::call('Boat@UpdateBoatAction', [new DataTransporter($request)]);

        return $this->json($boat);
    }

    /**
     * @param DeleteBoatRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBoat(DeleteBoatRequest $request)
    {
        Apiato::call('Boat@DeleteBoatAction', [new DataTransporter($request)]);

        return $this->noContent();
    }

    /**
     * @param CreateScheduleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSchedule(CreateScheduleRequest $request)
    {
        $schedule = Apiato::call('Boat@CreateScheduleAction', [new DataTransporter($request)]);

        return $this->created($this->transform($schedule, ScheduleTransFormer::class));
    }

    /**
     * @param FindScheduleRequest $request
     * @return array
     */
    public function findSchedule(FindScheduleRequest $request)
    {
        $schedule = Apiato::call('Boat@FindScheduleAction', [new DataTransporter($request)]);

        return $this->json($schedule);
    }

    /**
     * @param GetScheduleRequest $request
     * @return array
     */
    public function getSchedule(GetScheduleRequest $request)
    {
        $schedule = Apiato::call('Boat@GetScheduleAction', [new DataTransporter($request)]);

        return $this->transform($schedule, ScheduleTransformer::class);
    }

    /**
     * @param UpdateScheduleRequest $request
     * @return array
     */
    public function updateSchedule(UpdateScheduleRequest $request)
    {
        $schedule = Apiato::call('Boat@UpdateScheduleAction', [new DataTransporter($request)]);

        return $this->json($schedule);
    }

    /**
     * @param DeleteScheduleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSchedule(DeleteScheduleRequest $request)
    {
        Apiato::call('Boat@DeleteScheduleAction', [new DataTransporter($request)]);

        return $this->noContent();
    }

    public function initialNewSchedule(InitialNewScheduleRequest $request)
    {
        $init = Apiato::call('Boat@InitialNewScheduleAction', [new DataTransporter($request)]);
        
        return $this->json($init);
    }
}
