<?php

namespace App\Containers\Boat\UI\API\Transformers;

use App\Containers\Boat\Models\Schedule;
use App\Ship\Parents\Transformers\Transformer;

class ScheduleTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Schedule $entity
     *
     * @return array
     */
    public function transform(Schedule $entity)
    {
        $response = [
            'object' => 'Schedule',
            'id' => $entity->id,
            'boat_id' => $entity->boat_id,
            'timevals' => $entity->timevals,
            'departure_station_id' => $entity->departure_station_id,
            'arrival_station_id' => $entity->arrival_station_id,
            'days_of_week' => $entity->days_of_week,
            'price' => $entity->price,
            'description' => $entity->description,
            'active' => $entity->active,
            'boat' => $entity->boat,
            'departure' => $entity->departure_station,
            'arrival' => $entity->arrival_station,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
