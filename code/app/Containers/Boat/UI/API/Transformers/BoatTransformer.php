<?php

namespace App\Containers\Boat\UI\API\Transformers;

use App\Containers\Boat\Models\Boat;
use App\Ship\Parents\Transformers\Transformer;

class BoatTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Boat $entity
     *
     * @return array
     */
    public function transform(Boat $entity)
    {
        $response = [
            'object' => 'Boat',
            'id' => $entity->id,
            'name' => $entity->name,
            'num_of_seats' => $entity->num_of_seats,
            'active' => $entity->active,
            'description' => $entity->description,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
