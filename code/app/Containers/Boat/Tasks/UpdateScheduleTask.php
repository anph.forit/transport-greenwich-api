<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\ScheduleRepository;
use App\Containers\Boat\Models\Schedule;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateScheduleTask extends Task
{

    protected $repository;

    public function __construct(ScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        if (empty($data)) {
            throw new UpdateResourceFailedException('Inputs are empty.');
          }
          try {
              if (!is_null($data['id'])){
                  if (isset($data['active']) && $data['active'] == true){
                    $schelst =  $this->repository->findWhere([
                        'id' => $data['id']
                      ]);
                    Schedule::where('boat_id', $schelst[0]->boat_id)->update(array_filter(["active" => false], function($var){return $var !== null;}));
                  }
                  return Schedule::where('id', $data['id'])->update(array_filter($data, function($var){return $var !== null;}));
              }
              
          } catch (Exception $exception) {
              throw new UpdateResourceFailedException();
          }
          return NULL;
    }
}
