<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\ScheduleRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\Boat\Models\Schedule;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateScheduleTask extends Task
{

    protected $repository;

    public function __construct(ScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        if (empty($data)) {
            throw new CreateResourceFailedException('Inputs are empty.');
          }
          try {
            Schedule::where('boat_id', $data['boat_id'])->update(array_filter(["active" => false], function($var){return $var !== null;}));
            return $this->repository->create($data);
          } catch (Exception $exception) {
            var_dump($exception);
      
              throw new CreateResourceFailedException();
          }
          return NULL;
    }
}
