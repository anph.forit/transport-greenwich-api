<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\BoatRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateBoatTask extends Task
{

    protected $repository;

    public function __construct(BoatRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        if (empty($data)) {
            throw new CreateResourceFailedException('Inputs are empty.');
          }
          try {
            return $this->repository->create($data);
          } catch (Exception $exception) {
            var_dump($exception);
      
              throw new CreateResourceFailedException();
          }
          return NULL;
    }
}
