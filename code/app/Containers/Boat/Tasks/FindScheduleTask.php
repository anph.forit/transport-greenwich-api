<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\ScheduleRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindScheduleTask extends Task
{

    protected $repository;

    public function __construct(ScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $val)
    {
        if (isset($val['id']) && $val['id'] != null){
            $schelst =  $this->repository->findWhere([
              'id' => $val['id']
            ]);
          }
          else{
            $sqlstr = "";
            $chk = 0;
            if (isset($val['boat_id']) && $val['boat_id'] != null){
              $sqlstr = " and boat_id = '".$val['boat_id']."'";
              $chk = 1;
            }
            if (isset($val['departure_station_id']) && $val['departure_station_id'] != null){
              $sqlstr = $sqlstr." and departure_station_id = '".$val['departure_station_id']."'";
              $chk = 1;
            }
            if (isset($val['arrival_station_id']) && $val['arrival_station_id'] != null){
              $sqlstr = $sqlstr." and arrival_station_id = '".$val['arrival_station_id']."'";
              $chk = 1;
            }
            if (isset($val['dow']) && $val['dow'] != null){
              $sqlstr = $sqlstr." and days_of_week LIKE '%".$val['dow']."%'";
              $chk = 1;
            }
            if ($chk == 1){
              $sqlstr = substr($sqlstr, 5, strlen($sqlstr) - 1);
            }
            $schelst = $this->repository->with('boat')->with('departure_station')->with('arrival_station')->scopeQuery(function ($query) use($sqlstr) {
                return $query->where(function ($sq) use ($sqlstr) {
                    $sq->whereRaw($sqlstr);});})->all();
          }
          if ($schelst)
            return $schelst;
          return NULL;
    }
}
