<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\BoatRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteBoatTask extends Task
{

    protected $repository;

    public function __construct(BoatRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
