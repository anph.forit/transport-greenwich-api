<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\ScheduleRepository;
use App\Ship\Parents\Tasks\Task;

class GetScheduleTask extends Task
{

    protected $repository;

    public function __construct(ScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->with('boat')->with('departure_station')->with('arrival_station')->orderBy('boat_id')->paginate();
    }
}
