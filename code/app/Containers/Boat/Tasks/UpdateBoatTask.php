<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\BoatRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Containers\Boat\Models\Boat;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateBoatTask extends Task
{

    protected $repository;

    public function __construct(BoatRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data)
    {
        if (empty($data)) {
            throw new UpdateResourceFailedException('Inputs are empty.');
          }
          try {
              if (!is_null($data['id'])){
                return Boat::where('id', $data['id'])->update(array_filter($data, function($var){return $var !== null;}));
              }
              // else if (!is_null($data['parent_id'])){
              //   return Location::where('parent_id', $data['parent_id'])->update(array_filter($data, function($var){return $var !== null;}));
              // }
              
          } catch (Exception $exception) {
              throw new UpdateResourceFailedException();
          }
          return NULL;
    }
}
