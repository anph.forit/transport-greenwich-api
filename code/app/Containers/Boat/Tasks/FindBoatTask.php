<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\BoatRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindBoatTask extends Task
{

    protected $repository;

    public function __construct(BoatRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $val)
    {
        if (isset($val['id']) && $val['id'] != null){
            $boatlst =  $this->repository->findWhere([
              'id' => $val['id']
            ]);
          }
          else{
            if ($val['val'] == "all"){
              $boatlst =  $this->repository->findWhere([
                'active' => true
              ]);
            }
            else{
              $boatlst = $this->repository->scopeQuery(function ($query) use($val) {
                return $query->where(function ($sq) use ($val) {
                    $sq->whereRaw("LOWER(name) LIKE '%".strtolower($val['val'])."%'");
                })->where('active', true);})->all();
            }
            
          }
          if ($boatlst)
            return $boatlst;
          return NULL;
    }
}
