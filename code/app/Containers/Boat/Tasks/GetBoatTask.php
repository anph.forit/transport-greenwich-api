<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\BoatRepository;
use App\Ship\Parents\Tasks\Task;

class GetBoatTask extends Task
{

    protected $repository;

    public function __construct(BoatRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
