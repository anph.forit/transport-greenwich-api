<?php

namespace App\Containers\Boat\Tasks;

use App\Containers\Boat\Data\Repositories\ScheduleRepository;
use App\Ship\Exceptions\NotFoundException;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Tasks\Task;
use Exception;

class InitialNewScheduleTask extends Task
{

    protected $repository;

    public function __construct(ScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $boatlst = Apiato::call('Boat@GetBoatTask');
        $stationlst = Apiato::call('Location@GetStationTask');
        $schedulelst = Apiato::call('Boat@GetScheduleTask');
        $res             = new \stdClass;
        $res->boat_list       = $boatlst;
        $res->station_list     = $stationlst;
        $res->schedule_list     = $schedulelst;
        return $res;
    }
}
