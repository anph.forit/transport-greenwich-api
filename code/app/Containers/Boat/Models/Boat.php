<?php

namespace App\Containers\Boat\Models;

use App\Ship\Parents\Models\Model;

class Boat extends Model
{
    protected $fillable = [
        'id',
        'name',
        'num_of_seats',
        'active',
        'description',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'boats';
}
