<?php

namespace App\Containers\Boat\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Location\Models\Station;

class Schedule extends Model
{
    protected $fillable = [
        'id',
        'boat_id',
        'timevals',
        'departure_station_id',
        'arrival_station_id',
        'days_of_week',
        'price',
        'description',
        'active',
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function boat()
    {
        return $this->hasOne(Boat::class, 'id', 'boat_id');
    }

    public function departure_station()
    {
        return $this->hasOne(Station::class, 'id', 'departure_station_id');
    }

    public function arrival_station()
    {
        return $this->hasOne(Station::class, 'id', 'arrival_station_id');
    }
}
