<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class GetBoatAction extends Action
{
    public function run(DataTransporter $data)
    {
        $boats = Apiato::call('Boat@GetBoatTask');

        return $boats;
    }
}
