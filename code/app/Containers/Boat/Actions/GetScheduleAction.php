<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class GetScheduleAction extends Action
{
    public function run(DataTransporter $data)
    {
        $schedules = Apiato::call('Boat@GetScheduleTask');

        return $schedules;
    }
}
