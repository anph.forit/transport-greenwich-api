<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class FindScheduleAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'id'       => $data->id,
            'boat_id' => $data->boat,
            'departure_station_id' => $data->departure,
            'arrival_station_id' => $data->arrival,
            'dow' => $data->dow,
          ];
        $schedule = Apiato::call('Boat@FindScheduleTask', [$params]);

        return $schedule;
    }
}
