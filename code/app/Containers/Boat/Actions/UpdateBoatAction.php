<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateBoatAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'id'                => $data->id,
            'name'              => $data->name,
            'num_of_seats'      => $data->num_of_seats,
            'description'       => $data->description,
            'active'            => $data->active,
          ];
        // remove empty value and key
        $params = array_filter($params, function($x) { return !is_null($x); });

        $boat = Apiato::call('Boat@UpdateBoatTask', [$params]);

        return $boat;
    }
}
