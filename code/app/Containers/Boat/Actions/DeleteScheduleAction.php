<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteScheduleAction extends Action
{
    public function run(DataTransporter $data)
    {
        return Apiato::call('Boat@DeleteScheduleTask', [$request->id]);
    }
}
