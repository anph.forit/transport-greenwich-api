<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateScheduleAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'id'                    => $data->id,
            'boat_id'               => $data->boat_id,
            'timevals'              => $data->timevals,
            'departure_station_id'  => $data->departure_station_id,
            'arrival_station_id'    => $data->arrival_station_id,
            'days_of_week'          => $data->days_of_week,
            'price'                 => $data->price,
            'description'           => $data->description,
            'active'                => $data->active,
          ];
        // remove empty value and key
        $params = array_filter($params, function($x) { return !is_null($x); });

        $schedule = Apiato::call('Boat@UpdateScheduleTask', [$params]);

        return $schedule;
    }
}
