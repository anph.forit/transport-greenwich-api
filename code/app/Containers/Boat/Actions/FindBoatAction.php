<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class FindBoatAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'id'       => $data->id,
            'val' => $data->val
          ];
        $boat = Apiato::call('Boat@FindBoatTask', [$params]);

        return $boat;
    }
}
