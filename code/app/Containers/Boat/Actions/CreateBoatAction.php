<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateBoatAction extends Action
{
    public function run(DataTransporter $data)
    {
        $params = [
            'name'              => $data->name,
            'num_of_seats'      => $data->num_of_seats,
            'description'       => $data->description,
            'active'            => true,
          ];
          // remove empty value and key
        $params = array_filter($params);

        $boat = Apiato::call('Boat@CreateBoatTask', [$params]);

        return $boat;
    }
}
