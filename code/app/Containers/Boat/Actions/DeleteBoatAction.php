<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteBoatAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Boat@DeleteBoatTask', [$request->id]);
    }
}
