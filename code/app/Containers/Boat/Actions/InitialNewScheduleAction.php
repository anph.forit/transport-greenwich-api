<?php

namespace App\Containers\Boat\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\Apiato;

class InitialNewScheduleAction extends Action
{
    public function run(DataTransporter $data)
    {       
        $initial = Apiato::call('Boat@InitialNewScheduleTask');

        return $initial;
    }
}
